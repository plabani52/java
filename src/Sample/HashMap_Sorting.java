package Sample;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class HashMap_Sorting {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	    HashMap<Integer, String> hashmap = new HashMap<Integer, String>();
	    
	    hashmap.put(22,"A");
	    hashmap.put(55,"B");
	    hashmap.put(33,"Z");
	    hashmap.put(44,"M");
	    hashmap.put(99,"I");
	    hashmap.put(88,"X");
	    
	    Set set = hashmap.entrySet();//entryset is use to access our hashmap,so that perform any action on hashmap
	    Iterator iterator = set.iterator();

	    while(iterator.hasNext()) {
	        Map.Entry xyz = (Map.Entry)iterator.next();
	        System.out.print(xyz.getKey() + ": ");
	        System.out.println(xyz.getValue());
	    }
System.out.println("----------------------");
	   TreeMap<Integer, String> tree=new TreeMap<>(hashmap);
	   Set set1 = tree.entrySet();//entryset is use to access our hashmap,so that perform any action on hashmap
	    Iterator iterator1 = set1.iterator();

	    while(iterator1.hasNext()) {
	        Map.Entry xyz = (Map.Entry)iterator1.next();
	        System.out.print(xyz.getKey() + ": ");
	        System.out.println(xyz.getValue());
	}

}

}