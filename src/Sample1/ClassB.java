package Sample1;

public class ClassB extends ClassA {
	
	ClassB()
	{
		System.out.println("Default-Child");
	}
	ClassB(int a)
	{
		this("Plabani");

		System.out.println("INT-Child");
	}
	
	ClassB(String a)
	{
		super(10);
		System.out.println("String-Child");
	}
	
	public void test3(int a)
	{
		System.out.println("test3-Override-Child");
	}

}
