package Sample1;

public class ClassA {
	ClassA()
	{
		System.out.println("Default");
	}
	
	ClassA(int a)
	{
		System.out.println("INT");
	}
	
	ClassA(String a)
	{
		System.out.println("String");
	}
	
	public void test(int a)
	{
		this.test("Plabani");
		System.out.println("INT Method");
	}
	
	public void test(String a)
	{
		System.out.println("String Method");
	}
	
	public void test3(String a)
	{
		System.out.println("test3-Override-Parent");
	}
}
