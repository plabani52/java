
public class Method_overloading1 {
	
	Method_overloading1(int a)
	{
		this("Plabani");
		System.out.println("This is INT-CONS");

	}
	Method_overloading1(String b)
	{
		System.out.println("This is String-CONS");

	}
	
	public void test(int a)
	{
		this.test("Plabani");
		System.out.println("This is INT");
	}
	
	public void test(String a)
	{
		System.out.println("This is String");

	}
	
	public void test(double b)
	{
		this.test(20);
		System.out.println("This is doble");

	}
	
	

}
