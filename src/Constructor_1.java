
public class Constructor_1 {

	Constructor_1()
	{
		System.out.println("Default Constructor");
		
	}
	Constructor_1(int a)
	{
		this("Plabani");
		System.out.println("INT Constructor");
		
	}
	
	Constructor_1(String a)
	{
		System.out.println("String Constructor");
		
	}
	
	public static void test()
	{
		System.out.println("it's static method");
	}
	
	public void test1()
	{
		System.out.println("it's non-static method");
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Constructor_1 ref=new Constructor_1(20);
		Constructor_1.test();
		ref.test1();
		
	}

}
