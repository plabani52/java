
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		{
			 //Converting sublass object to Superclass
			Method_Overriding REF=new Method_Overriding1();
			REF.test("Plabani");
			REF.test1("Plabani");
			
			//converting upcasted ref variable to subclass,
			
			Method_Overriding1 ref1=(Method_Overriding1)REF;
			ref1.test5(20);
			ref1.test(20);
			ref1.test("Plabani");
			ref1.test(20);
	}


	}
}
