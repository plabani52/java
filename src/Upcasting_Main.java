
public class Upcasting_Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//Converting subclass object to Super class
	Upcasting_Parent ref=new Upcasting_Child();
	ref.test();
	ref.test1();
	
	//Converting upcasted ref variable to subclass object,is called as Downcasting
	
	Upcasting_Child ref1=(Upcasting_Child)ref;
	ref1.test();
	ref1.test1();
	
	} 

}
