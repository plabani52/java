
public class Method_OverLoading {

	Method_OverLoading(int a)
	{
		this("Plabani");
		System.out.println("INT-CONS");
		
	}
	
	Method_OverLoading(String b)
	{
		System.out.println("String-CONS");

	}
	public void test(int a,int b)
	{
		this.test("Plabani", 20);
		System.out.println("INT");
	}
	
	public void test(String a,int b)
	{
		System.out.println("String");
	}

	public static void main(String args[])
	{
		Method_OverLoading ref=new Method_OverLoading(10);
		ref.test(20, 20);
		
	}
}
