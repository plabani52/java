
public abstract class AbPrac {

	AbPrac()
	{
		System.out.println("This is Default constructor");
	}
	//Abstract method,which has method declaration but definition not present
	abstract void test();
	
	
	//Concrete,(Method declaration,definition both present)
	public void test1()
	{
	System.out.println("This is concrete");	
		
	}
	}

